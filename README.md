# Binary Search tree [TypeScipt]
Class BSTree features:
* Node value is generic
* .add(nodeValue), linked
* .travers(() => void), in-order
* .min() and .max(), both return Node or nulla