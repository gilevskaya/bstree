interface TNode<T> {
  X: T
  P: TNode<T> 
  L: TNode<T> 
  R: TNode<T>
}

enum CompRes {
  less = -1, same = 0, more = 1
}

interface ICompFunc<T> {
  (a: T, b: T): CompRes
}

function defaultCompFunc<T> (a: T, b: T) {
  if (a < b) return CompRes.less
  else if (a > b) return CompRes.more
  else return CompRes.same
}



class BSTree<T> {
  private root: TNode<T>
  private compare: ICompFunc<T>

  constructor(compFunc?: ICompFunc<T>) {
    this.root = null
    this.compare = compFunc || defaultCompFunc
  }

  public add(val: T): BSTree<T> {
    let node = { X: val, P: null, L: null, R: null }
    if (this.insertNode(node) !== null) return this
    else return null
  }

  public del(val: T): boolean {
    return true
  }

  public min(): TNode<T> {
    return this.minR(this.root)
  }

  public max(): TNode<T> {
    return this.maxR(this.root)
  }

  public traversal(cb?: (node: TNode<T>) => void): void {
    this.inorderTraversal(this.root, cb)
  }

  ////////////////// PRIVATE //////////////////

  private insertNode(node: TNode<T>): TNode<T> {
    let parent: TNode<T> = null
    let cur: TNode<T> = this.root
    let cmp: CompRes

    // searchig for the parent element for node
    while (cur != null) {
      cmp = this.compare(node.X, cur.X)
      if (cmp === CompRes.less) {
        parent = cur; cur = cur.L
      } else if (cmp === CompRes.more) {
        parent = cur; cur = cur.R
      } else return null
    }

    node.P = parent

    if (parent === null) {
      // tree is empty
      this.root = node
    } else if (this.compare(parent.X, node.X) === CompRes.less) {
      parent.R = node
    } else parent.L = node

    return node
  }


  private minR(curRoot: TNode<T>): TNode<T> {
    if (curRoot.L == null) return curRoot
    return this.minR(curRoot.L)
  }

  private maxR(curRoot: TNode<T>): TNode<T> {
    if (curRoot.R == null) return curRoot
    return this.maxR(curRoot.R)
  }


  private inorderTraversal(curRoot: TNode<T>, cb?: (node: TNode<T>) => void): void {
    if (curRoot == null) return
    this.inorderTraversal(curRoot.L, cb)
    if (cb) cb(curRoot)
    this.inorderTraversal(curRoot.R, cb)
  }


  private swap(n1: TNode<T>, n2: TNode<T>): void {
    // if one of the nodes was root
    if (n1.P == null) {
      this.root == n2
    } else if (n2.P == null) {
      this.root == n1
    }


  }
  
  ///////////////// PRITINING /////////////////

  public print(): void {
    console.log(this.root)
  }
}





function printNode(node: TNode<number>): void {
  // console.log('::', node)
  let pr = (node !== null && node !== undefined) ? `[${node.X}]` : `[X]`
  process.stdout.write(pr + ' ')
}

// let t = new BSTree<number>()
// t.add(22).add(10).add(33).add(15).add(7)
// t.print()
// t.traversal(printNode); console.log()
// console.log(printNode(t.min()))